

getForecast();
function getForecast() {
    const apiURL = 'https://api.openweathermap.org/data/2.5/forecast?id=2816884&units=metric&APPID=4d740699e37a38e6fd92554001fa0cb3';
    let weekday = [];

    weekday[0]="Mo";
    weekday[1]="Di";
    weekday[2]="Mi";
    weekday[3]="Do";
    weekday[4]="Fr";
    weekday[5]="Sa";
    weekday[6]="So";

    fetch(apiURL)
        .then(function (response) {
            return response.json();
        })
        .then(function (jsonObject) {
            //console.table(jsonObject);

            for (let i = 0; i < jsonObject.list.length; i++) {

                if (jsonObject.list[i].dt_txt.includes("18:00:00")) {
                    let div = document.createElement('section');
                    let h3 = document.createElement('h3');
                    let image = document.createElement('img');
                    let pTemp = document.createElement('p');
                    let d = new Date(jsonObject.list[i].dt * 1000);

                    h3.textContent = weekday[d.getDay()];
                    image.setAttribute("src","https://openweathermap.org/img/w/" + jsonObject.list[i].weather[0].icon + ".png");
                    image.setAttribute("alt", jsonObject.list[i].weather[0].description)
                    pTemp.innerHTML = Math.round(jsonObject.list[i].main.temp) + " °C";
                    div.appendChild(h3);
                    div.appendChild(image);
                    div.appendChild(pTemp);
                    document.getElementById("forecast").appendChild(div);
                }
            }
        });
}
