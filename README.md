Mashup-App mit Node.js



Service: Die Single-Page App richtet sich an Landwirte und Hobby-Gärtner im Vogelsbergkreis. Man bekommt das Wetter der nächsten 5 Tage angezeigt das
	zum Beispiel für die Planung der Heu-Ernte benötigt wird da man dafür mindetends 4 - 5 Tage benötigt. Außerdem werden die Mond Phasen des jeweiliges 
	Monats im dem man sich befindet angezeigt, ebenfalls wichtig für die Ernte, es gibt immer mehr die sich danach richten um einen besseren Ernte 
	Ertrag zu bekommen.



Locale-Installation:
			1: Node.js installieren https://nodejs.org/en/
			2: Node.js Eingabeaufforderung öffenen und mit 'npm install http-server -g' http-server installieren
			3: In den Ordner der Web-App navigieren und den http-server mit dem Command 'http-server' starten 
			4: Der Server startet und man bekommt die ip angezeigt.

Alternativ:
			1: Alle abhängigkeiten mit 'npm install' installieren
			2: danach 'npm init' um ein nmp-Paket einzurichten
			3: In den Ordner der Web-App navigieren und mit 'npm start' die Webapplikation local starten
